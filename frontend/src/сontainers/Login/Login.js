import React, {useState} from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import Link from '@material-ui/core/Link';
import {Link as RouterLink} from 'react-router-dom';
import Grid from '@material-ui/core/Grid';
import LockOpenIcon from '@material-ui/icons/LockOpen';
import Typography from '@material-ui/core/Typography';
import {makeStyles} from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import {useDispatch, useSelector} from "react-redux";
import FormField from "../../components/UI/Form/FormField";
import Alert from '@material-ui/lab/Alert';
import {loginUser} from "../../store/atcions/userActions";
// import FacebookLogin from "../../components/FacebookLogin/FacebookLogin";

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%',
    marginTop: theme.spacing(3),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
  alert: {
    marginTop: theme.spacing(3),
    width: "100%"
  }
}));

const Login = () => {
  const classes = useStyles();

  const [state, setState] = useState({
    username: '',
    password: '',
  });

  const error = useSelector(state => state.users.loginError);
  const dispatch = useDispatch();

  const inputChangeHandler = e => {
    setState(prevState => ({...prevState, [e.target.name]: e.target.value}));
  };

  const formSubmitHandler = e => {
    e.preventDefault();
    dispatch(loginUser({...state}));
  };

  return (
      <Container component="main" maxWidth="xs">
        <CssBaseline/>
        <div className={classes.paper}>
          <Avatar className={classes.avatar}>
            <LockOpenIcon/>
          </Avatar>
          <Typography component="h1" variant="h5">
            Sign in
          </Typography>
          {error && <Alert severity="error" className={classes.alert}>{error.error}</Alert>}
          <form
              className={classes.form}
              onSubmit={formSubmitHandler}
          >
            <Grid container spacing={2}>
              <FormField
                  label="Enter username or email"
                  onChange={inputChangeHandler}
                  value={state.username}
                  name="username"
                  required={true}
              />
              <FormField
                  label="Password"
                  onChange={inputChangeHandler}
                  value={state.password}
                  type="password"
                  required={true}
                  name="password"
              />
            </Grid>
            <Button
                type="submit"
                fullWidth
                variant="contained"
                color="primary"
                className={classes.submit}
            >
              Sign In
            </Button>
            {/*<FacebookLogin />*/}
            <Grid container justify="flex-end">
              <Grid item>
                <Link component={RouterLink} to="/register" variant="body2">
                  Or sign up
                </Link>
              </Grid>
            </Grid>
          </form>
        </div>
      </Container>
  );
}

export default Login;