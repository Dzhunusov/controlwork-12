import React from 'react';
import {useDispatch} from "react-redux";
import AddPhotoForm from "../../components/AddPhotoForm/AddPhotoForm";
import {createPhoto} from "../../store/atcions/photoActions";

const NewPhoto = () => {
  const dispatch = useDispatch();

  const createUserPhoto = data => {
    dispatch(createPhoto(data));
  };

  return (
      <>
        <h1>New Photo</h1>
        <AddPhotoForm onSubmit={createUserPhoto}/>
      </>
  );
};

export default NewPhoto;