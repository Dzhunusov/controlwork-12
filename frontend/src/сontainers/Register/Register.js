import React, {useState} from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import Link from '@material-ui/core/Link';
import {Link as RouterLink} from 'react-router-dom';
import Grid from '@material-ui/core/Grid';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import {makeStyles} from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import {useDispatch, useSelector} from "react-redux";
// import {registerUser} from "../../store/actions/usersActions";
import FormField from "../../components/UI/Form/FormField";
import {registerUser} from "../../store/atcions/userActions";
// import FacebookLogin from "../../components/FacebookLogin/FacebookLogin";

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(3),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

const Register = () => {
  const classes = useStyles();

  const [state, setState] = useState({
    username: '',
    email: '',
    password: '',
  });

  const error = useSelector(state => state.users.registerError);
  const dispatch = useDispatch();

  const inputChangeHandler = e => {
    setState(prevState => ({...prevState, [e.target.name]: e.target.value}));
  };

  const formSubmitHandler = e => {
    e.preventDefault();
    dispatch(registerUser({...state}));
  };

  const getFieldErrors = fieldName => {
    console.log(error)
    try {
      return error.errors[fieldName].message;
    }catch (e) {
      return undefined;
    }
  }

  return (
      <Container component="main" maxWidth="xs">
        <CssBaseline/>
        <div className={classes.paper}>
          <Avatar className={classes.avatar}>
            <LockOutlinedIcon/>
          </Avatar>
          <Typography component="h1" variant="h5">
            Sign up
          </Typography>
          <form
              className={classes.form}
              onSubmit={formSubmitHandler}
          >
            <Grid container spacing={2}>
              <FormField
                  label="Username"
                  onChange={inputChangeHandler}
                  value={state.username}
                  name="username"
                  error={getFieldErrors("username")}
              />
              <FormField
                  label="Email"
                  onChange={inputChangeHandler}
                  value={state.email}
                  name="email"
                  error={getFieldErrors("email")}
              />
              <FormField
                  label="Password"
                  onChange={inputChangeHandler}
                  value={state.password}
                  type="password"
                  name="password"
                  error={getFieldErrors("password")}
              />
            </Grid>
            <Button
                id="registerBtn"
                type="submit"
                fullWidth
                variant="contained"
                color="primary"
                className={classes.submit}
            >
              Sign Up
            </Button>
            {/*<FacebookLogin/>*/}
            <Grid container justify="flex-end">
              <Grid item>
                <Link component={RouterLink} to="/login" variant="body2">
                  Already have an account? Sign in
                </Link>
              </Grid>
            </Grid>
          </form>
        </div>
      </Container>
  );
}

export default Register;