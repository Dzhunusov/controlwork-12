import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {fetchUserPhotos} from "../../store/atcions/photoActions";
import Grid from "@material-ui/core/Grid";
import PhotoItem from "../../components/PhotoItem/PhotoItem";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import {Link} from "react-router-dom";
import {makeStyles} from "@material-ui/core";

const useStyles = makeStyles({
  header: {
    marginBottom: "30px"
  }
});

const UserPhotos = props => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const photos = useSelector(state => state.photos.userPhotos);
  const user = useSelector(state => state.users.user);

  useEffect(() => {
    dispatch(fetchUserPhotos(props.match.params.id));
  }, [dispatch]);

  return (
      <Grid container direction="column">
        <Grid item container className={classes.header} direction="row" justify="space-between" alignItems="center">
          <Grid item>
            <Typography variant="h4">
              User Photos
            </Typography>
          </Grid>
          {
            user && <Grid item>
              <Button color="primary" component={Link} to="/new_photo">Add new photo</Button>
            </Grid>
          }
        </Grid>
        <Grid container direction="row" spacing={2}>
          {photos.map(photo => {
            return <PhotoItem
                key={photo._id}
                image={photo.photo}
                title={photo.title}
                userId={photo.user}
            />
          })}
        </Grid>
      </Grid>
  );
};

export default UserPhotos;