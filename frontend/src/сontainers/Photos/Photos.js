import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {fetchPhotos} from "../../store/atcions/photoActions";
import Grid from "@material-ui/core/Grid";
import PhotoItem from "../../components/PhotoItem/PhotoItem";

const Photos = () => {
  const dispatch = useDispatch();
  const photos = useSelector(state => state.photos.photos);
  const user = useSelector(state => state.users.user);

  useEffect(() => {
    dispatch(fetchPhotos());
  }, [dispatch])

  return (
      <Grid container  direction="row" spacing={2}>
        {photos.map(photo => {
          return <PhotoItem
              key={photo._id}
              image={photo.photo}
              title={photo.title}
              userId={photo.user}
          />
        })}
      </Grid>
  );
};

export default Photos;