import React from "react";
import CssBaseline from "@material-ui/core/CssBaseline";
import AppToolbar from "./components/UI/AppToolbar/AppToolbar";
import Container from "@material-ui/core/Container";
import {Route, Switch} from "react-router";
import Photos from "./сontainers/Photos/Photos";
import Register from "./сontainers/Register/Register";
import Login from "./сontainers/Login/Login";
import {useSelector} from "react-redux";
import UserPhotos from "./сontainers/UserPhotos/UserPhotos";
import NewPhoto from "./сontainers/NewPhoto/NewPhoto";

function App() {
  const user = useSelector(state => state.users.user);

  return (
    <>
      <CssBaseline/>
      <AppToolbar user={user}/>
      <main>
        <Container>
          <Switch>
            <Route path="/" exact component={Photos}/>
            <Route path="/register" exact component={Register}/>
            <Route path="/login" exact component={Login}/>
            <Route path="/photos/:id" exact component={UserPhotos}/>
            <Route path="/new_photo" exact component={NewPhoto}/>
          </Switch>
        </Container>
      </main>
    </>
  );
}

export default App;
