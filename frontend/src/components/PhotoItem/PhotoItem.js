import React from 'react';
import Grid from "@material-ui/core/Grid";
import Card from "@material-ui/core/Card";
import CardActionArea from "@material-ui/core/CardActionArea";
import CardMedia from "@material-ui/core/CardMedia";
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";
import defaultImage from "../../accepts/images/default-image-620x600.jpg"
import {makeStyles} from "@material-ui/core";
import {apiUrl} from "../../constants";
import PropTypes from "prop-types";
import Modal from "@material-ui/core/Modal";
import {Link} from "react-router-dom";

const useStyles = makeStyles(theme => ({
  root: {
    maxWidth: 345,
    height: "100%"
  },
  media: {
    height: 0,
    padding: "56.25%"
  },
  paper: {
    position: 'absolute',
    width: "400px",
    backgroundColor: theme.palette.background.paper,
    border: '2px solid #000',
    boxShadow: theme.shadows[5],
    padding: theme.spacing(1),
    top: "14%",
    left: "30%",
  },
  modalImage: {
    width: "100%",
    height: "auto"
  }
}));

const PhotoItem = ({image, title, userId}) => {
  const classes = useStyles();

  let userImage = defaultImage;
  if (image) {
    userImage = apiUrl + "/uploads/" + image;
  }


  const [open, setOpen] = React.useState(false);

  const handleOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };




  return (
      <Grid item xs={3}>
        <Card className={classes.root}>
          <CardActionArea>
            <CardMedia
                className={classes.media}
                image={userImage}
                title={title}
                onClick={handleOpen}
            />
            <CardContent>
              <Typography gutterBottom variant="h5" component="h2">
                <Link to={"/photos/" + userId}>{title}</Link>
              </Typography>
              {/*<Typography variant="body2" color="textSecondary" component="p">*/}
              {/*  {user.username}*/}
              {/*</Typography>*/}
            </CardContent>
          </CardActionArea>
        </Card>
        <Modal
            open={open}
            onClose={handleClose}
            aria-labelledby="simple-modal-title"
            aria-describedby="simple-modal-description"
        >
          <div className={classes.paper}>
            <img src={userImage} alt={title} className={classes.modalImage}/>
          </div>
        </Modal>
      </Grid>
  );
};

PhotoItem.propTypes = {
  image: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  user: PropTypes.string,
}

export default PhotoItem;