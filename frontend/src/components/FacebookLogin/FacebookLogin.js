import React from 'react';
import FacebookLoginButton from "react-facebook-login/dist/facebook-login-render-props";
import {fbAppId} from "../../constants";
import Button from "@material-ui/core/Button";
import {useDispatch} from "react-redux";
// import {facebookLogin} from "../../store/actions/usersActions";

const FacebookLogin = () => {
  const dispatch = useDispatch();

  const FacebookResponse = response => {
    if(response.id){
      dispatch(facebookLogin(response))
    }
  };

  return <FacebookLoginButton
      appId={fbAppId}
      fields="name,email,picture"
      render={renderProps => {
        return <Button
            onClick={renderProps.onClick}
            variant="outlined"
            color="primary"
        >Enter with facebook</Button>
      }}
      callback={FacebookResponse}
  />;
};

export default FacebookLogin;