import React, {useRef, useState} from 'react';
import {makeStyles} from "@material-ui/core";
import Grid from "@material-ui/core/Grid";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";

const useStyle = makeStyles({
  input: {
    display: "none",
  }
})

const FileInput = ({onChange, name, label}) => {
  const classes = useStyle();
  const inputRef = useRef();
  const [fileName, setFileName] = useState('');

  const activateInput = () => {
    inputRef.current.click();
  };

  const onFileChange = e => {
    if(e.target.files[0]){
      setFileName(e.target.files[0].name);
    }else {
      setFileName("");
    }
    onChange(e);
  };

  return (
      <>
        <input
            type="file"
            name={name}
            ref={inputRef}
            className={classes.input}
            onChange={onFileChange}
        />
        <Grid container direction="row" spacing={2} alignItems="center">
          <Grid item xs>
            <TextField
                variant="outlined"
                disabled
                fullWidth
                label={label}
                value={fileName}
                onClick={activateInput}
            />
          </Grid>
          <Grid item>
            <Button variant="contained" onClick={activateInput}>Browse</Button>
          </Grid>
        </Grid>
      </>
  );
};

export default FileInput;