import React from 'react';
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import {makeStyles} from "@material-ui/core";
import {Link} from "react-router-dom";
import UserMenu from "../../Menu/UserMenu";
import AnonymousMenu from "../../Menu/AnonymousMenu";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    flexGrow: 1,
  },
  staticToolbar: {
    marginBottom: theme.spacing(2),
  },
  mainLink: {
    color: "inherit",
    textDecoration: "none",
    "&:hover": {
      color: "inherit",
    }
  },
  dropdownBtn: {
    color: "#FFFFFF"
  }
}));

const AppToolbar = ({user}) => {
  const classes = useStyles();

  return (
      <div className={classes.root}>
        <AppBar position="static">
          <Toolbar>
            <Typography variant="h6" className={classes.title}>
              <Link to="/" className={classes.mainLink}>Gallery</Link>
            </Typography>
            {user ? <UserMenu className={classes.dropdownBtn} user={user}/> : <AnonymousMenu/> }
          </Toolbar>
        </AppBar>
        <Toolbar className={classes.staticToolbar}/>
      </div>
  );
};

export default AppToolbar;