import React from 'react';
import Button from "@material-ui/core/Button";
import {Link} from "react-router-dom";

const AnonymousMenu = () => {
  return (
      <>
        <Button color="inherit" component={Link} to="/register">Sign Up</Button>
        <Button color="inherit" component={Link} to="/login">Sign In</Button>
      </>
  );
};

export default AnonymousMenu;