import React, {useState} from 'react';
import {makeStyles} from "@material-ui/core/styles";
import FormControl from "@material-ui/core/FormControl";
import Button from "@material-ui/core/Button";
import FileInput from "../../components/UI/Form/FileInput";
import FormField from "../../components/UI/Form/FormField";

const useStyles = makeStyles((theme) => ({
  root: {
    '& > *': {
      margin: theme.spacing(1),
      width: '100%',
    },
  },
}));

const AddPhotoForm = ({onSubmit}) => {
  const classes = useStyles();

  const [state, setState] = useState({
    title: '',
    photo: '',
  });

  const submitFormHandler = e => {
    e.preventDefault();
    const formData = new FormData();

    Object.keys(state).forEach(key => {
      formData.append(key, state[key]);
    });
    onSubmit(formData);
  };

  const inputChangeHandler = e => {
    const name = e.target.name;
    const value = e.target.value;
    setState(prevState => ({
      ...prevState,
      [name]: value,
    }));
  };

  const fileChangeHandler = e => {
    const name = e.target.name;
    const file = e.target.files[0];

    setState(prevState => ({...prevState, [name]: file}));
  };

  return (
      <form
          onSubmit={submitFormHandler}
          className={classes.root}
          noValidate
          autoComplete="off"
      >
        <FormField
            label="Product title"
            onChange={inputChangeHandler}
            value={state.title}
            name="title"
            required={true}
        />
        <FormControl fullWidth className={classes.margin} variant="outlined">
          <FileInput
              label="Photo"
              name="photo"
              onChange={fileChangeHandler}
          />
        </FormControl>
        <FormControl fullWidth className={classes.margin} variant="outlined">
          <Button color="primary" type="submit">Create</Button>
        </FormControl>
      </form>
  );
};

export default AddPhotoForm;
