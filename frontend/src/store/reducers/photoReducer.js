import {FETCH_PHOTOS_SUCCESS, FETCH_USERPHOTOS_SUCCESS} from "../actionsType";

const initialState = {
  photos: [],
  userPhotos: [],
};

const photoReducer = (state = initialState, action) => {
  switch(action.type){
    case FETCH_PHOTOS_SUCCESS:
      return {...state, photos: action.photos};
    case FETCH_USERPHOTOS_SUCCESS:
      return {...state, userPhotos: action.userPhotos};
    default:
      return state;
  }
};

export default photoReducer;