import {CREATE_PHOTO_SUCCESS, FETCH_PHOTOS_SUCCESS, FETCH_USERPHOTOS_SUCCESS} from "../actionsType";
import axiosApi from "../../axiosApi";
import {push} from "connected-react-router";

const fetchPhotosSuccess = photos => {
  return {type: FETCH_PHOTOS_SUCCESS, photos};
};

export const fetchPhotos = () => {
  return async dispatch => {
    const response = await axiosApi.get("/photos");
    dispatch(fetchPhotosSuccess(response.data));
  }
};

const fetchUserPhotosSuccess = userPhotos => {
  return {type: FETCH_USERPHOTOS_SUCCESS, userPhotos};
};

export const fetchUserPhotos = userId => {
  return async dispatch => {
    const response = await axiosApi.get("/photos?user="+ userId);
    dispatch(fetchUserPhotosSuccess(response.data));
  }
};

const createPhotoSuccess = () => {
  return {type: CREATE_PHOTO_SUCCESS};
};

export const createPhoto = (photoData) => {
  return (dispatch, getState)=> {
    const headers = {"Authorization" : getState().users.user.token};
    return  axiosApi.post("/photos", photoData, {headers}).then(() => {
      dispatch(createPhotoSuccess());
      dispatch(push("/"));
    });
  }
}