const router = require("express").Router();
const User = require("../models/User");
const congif = require("../config");
const axios = require("axios");

router.post("/", async (req, res) => {
  const user = new User(req.body);
  try {
    user.generateToken();
    await user.save();
    res.send(user);
  } catch (e) {
    return res.status(400).send(e);
  }
});

router.post("/sessions",async (req, res) => {

  const user = await User.findOne({username: req.body.username});
  if(!user){
    return res.status(400).send({message: "Username not found"});
  }

  const password = await user.checkPassword(req.body.password);
  if(!password){
    return res.status(400).send({message: "Password wrong"});
  }

  user.generateToken();
  await user.save({validateBeforeSave: false});
  res.send(user);
});

router.delete("/sessions", async (req, res) => {
  const token = req.get("Authorization");
  const success = {message: "Success"};

  if(!token) return res.send(success);
  const user = await User.findOne({token});
  if(!user) return res.send(success);

  user.generateToken();
  await user.save({validateBeforeSave: false});
  return res.send(success);
});

router.post("/facebookLogin", async (req, res) => {
  const inputToken = req.body.accessToken;
  const accessToken = config.fb.appId + "|" + config.fb.appSecret;
  const debugTokenUrl = `https://graph.facebook.com/debug_token?input_token=${inputToken}&access_token=${accessToken}`;

  try{
    const response = await axios.get(debugTokenUrl);
    if(response.data.data.error){
      return res.status(401).send({message: "Facebook token incorrect"})
    }
    if(req.body.id !== response.data.data.user_id){
      return res.status(401).send({message: "Wrong userID"});
    }

    let user = await User.findOne({facebookId: req.body.id});
    console.log(req.body);
    if(!user){
      user = new User({
        username: req.body.email,
        password: nanoid(),
        email: req.body.email,
        facebookId: req.body.id,
        displayName: req.body.name
      })
    }
    user.generateToken();
    await user.save({validateBeforeSave: false});
    res.send(user);
  }catch (e) {
    console.log(e);
    return res.status(401).send({message: "Facebook token incorrect"});
  }
})

module.exports = router;