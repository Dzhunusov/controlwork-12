const router = require("express").Router();
const multer = require("multer");
const {nanoid} = require("nanoid");
const path = require("path");
const config = require("../config");
const Photo = require("../models/Photo");

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadsPath);
  },
  filename: (req, file, cb) => {
    cb(null, nanoid() + path.extname(file.originalname));
  }
});

const uploads = multer({storage});


router.get("/", async (req, res) => {
  let query;
  if(req.query.user){
    query = {user: req.query.user};
  }
  try{
    const photos = await Photo.find(query);
    res.send(photos);
  }catch (e) {
    res.status(500).send(e)
  }
});

router.post("/", [auth], uploads.single("photo"), async (req, res) => {
  const photoData = req.body;

  if(req.file){
    photoData.photo = req.file.filename;
  }

  photoData.user = req.user.username;

  const newPhotoData = new Photo(photoData);
  try{
    await newPhotoData.save();
    res.send(newPhotoData);
  }catch (e) {
    res.status(400).send(e);
  }
});

module.exports = router;
