const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
const config = require("./config");
const app = express();
const photos = require("./app/photos");
const users = require("./app/users");
const port = 8000;

app.use(cors());
app.use(express.json());
app.use(express.static("public"));

const run = async () => {
  await mongoose.connect(config.db.url + "/" + config.db.name, {useNewUrlParser: true, autoIndex: true});

  app.use("/users", users);
  app.use("/photos", photos);


  console.log("Connected to mongo DB");
  app.listen(port, () => {
    console.log(`Server started at http://localhost:${port}`)
  });
};

run().catch(console.log);