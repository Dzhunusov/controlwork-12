const mongoose = require("mongoose");
const config = require("./config");
const {nanoid} = require("nanoid");
const User = require("./models/User");
const Photo = require("./models/Photo");

mongoose.connect(config.db.url + "/" + config.db.name, {useNewUrlParser: true});

const db = mongoose.connection;

db.once("open", async () => {
  try{
    await db.dropCollection("users");
    await db.dropCollection("photos");
  }catch (e) {
    console.log("Collection were not presented, skipping drop...");
  }

  const[PerterP, HarryP] =  await User.create({
    username: "Peter Parker",
    email: "peter.P@gmail.com",
    password: "qazWSX29",
    token: nanoid(),

  },{
    username: "Harry Potter",
    email: "harry.P@gmail.com",
    password: "wsxEDC30",
    token: nanoid(),
  })

  await Photo.create({
    title: "Van Gogh. Sunflowers",
    photo: "Van_Gogh.jpg",
    user: PerterP._id
  },{
    title: "Da vinci. Mona Lisa",
    photo: "Mona_Lisa.jpg",
    user: HarryP._id,
  });

  db.close();
})
