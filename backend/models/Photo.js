const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const PhotoSchema = new Schema({
  title: {
    type: String,
    required:[true, "Поле title обязательно для заполения"],
  },
  photo: {
    type: String,
    required:[true, "Поле photo обязательно для заполнения"],
  },
  user: {
    type: Schema.Types.ObjectId,
    ref: "User",
    required: true,
  }
});

const Photo = mongoose.model("Photo", PhotoSchema);
module.exports = Photo;