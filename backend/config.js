const path = require("path");

const rootPath = __dirname;

module.exports = {
  rootPath,
  uploadsPath: path.join(rootPath, "public/uploads"),
  db: {
    name: "gallery",
    url: "mongodb://localhost"
  },
  fb: {
    appId: "702409463974523",
    appSecret: process.env.FB_SECRET
  }
}